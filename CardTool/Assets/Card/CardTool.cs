using System.Collections.Generic;
using SFB;
using UnityEngine;
using UnityEngine.UI;

public class CardTool : MonoBehaviour
{
    // Start is called before the first frame update
    public List<CardView> Cards = new List<CardView>();

    public Dropdown DCardType;
    public List<Toggle> TRarities;

    public Toggle TATK;
    public Toggle TMana;
    public Toggle THP;
    public Toggle TDur;
    public Toggle TTurn;

    public InputField IATK;

    public InputField IMana;

    public InputField IHP;

    public InputField IDur;

    public InputField ITurn;

    public Slider SScale;

    public UIDraggable UIDraggable;

    void Start()
    {
        this.UIDraggable.OnChangePosition = ChangePositionIcon;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeName(string name)
    {
        foreach (var card in Cards)
        {
            card.ChangeName(name);
            //card.ChangeIcon(name);
        }
    }

    public void ChangeGroup(string group)
    {
        foreach (var card in Cards)
        {
            card.ChangeGroup(group);
        }
    }

    public void ChangeCardType(string a)
    {
        Debug.Log("ChangeCardType " + DCardType.value);
        var cardType = (CardType)(DCardType.value + 1);
        foreach (var card in Cards)
        {
            card.ChangeCardType(cardType);
        }
    }

    public void ChangeRarity()
    {
        var r = 0;
        foreach(var c in TRarities){
            if(c.isOn){
                break;
            }else{
                r++;
            }
        }
        var rarity = (RarityType)(r);
        Debug.Log("rarity " +rarity);
        foreach (var card in Cards)
        {
            card.ChangeRarity(rarity);
        }
    }

    public void ChangeStylist()
    {
        foreach (var card in Cards)
        {
            card.ChangeStylist();
        }
    }

    public void ChangeATK(){
        this.ChangeParameterLeft(ParameterType.ATK,this.IATK.text,this.TATK.isOn);
    }

    public void ChangeMana(){
        this.ChangeParameterLeft(ParameterType.Mana,this.IMana.text,this.TMana.isOn);
    }
    public void ChangeDEF(){
        if(this.THP.isOn) this.ChangeParameterRight(ParameterType.HP,this.IHP.text);
        if(this.TDur.isOn) this.ChangeParameterRight(ParameterType.durability,this.IDur.text);
        if(this.TTurn.isOn) this.ChangeParameterRight(ParameterType.effect,this.ITurn.text);
    }

    public void ChangeParameterLeft(ParameterType parameterType, string value, bool active = true)
    {
        foreach (var card in Cards)
        {
            card.ChangeParameterLeft(parameterType, value, active);
        }
    }

    public void ChangeParameterRight(ParameterType parameterType, string value)
    {
        foreach (var card in Cards)
        {
            card.ChangeParameterRight(parameterType, value);
        }
    }

    public void ChangeContent(string content)
    {
        foreach (var card in Cards)
        {
            card.ChangeContent(content);
        }
    }

    public void ChangeIcon(string cardName)
    {
        foreach (var card in Cards)
        {
            card.ChangeContent(cardName);
        }
    }

    public void ChangeIconFromArt(string path)
    {
        foreach (var card in Cards)
        {
            card.ChangeIconFromArt(path);
        }
    }

    public void ChangeScale(float scale){
       foreach (var card in Cards)
        {
            card.ChangeScale(scale);
        }
    }

    public void ChangePositionIcon(Vector3 pos)
    {
        foreach (var card in Cards)
        {
            card.ChangePositionIcon(pos);
        }
    }

    public void ChooseFile()
    {
        var paths = StandaloneFileBrowser.OpenFilePanel("Open File", "", "", false);
        if (paths.Length != 0)
        {
            this.SScale.value = 1;
            foreach (var card in Cards)
            {
                card.ChangeIconFromArt(paths[0]);
            }
        }
        return;
    }
}
