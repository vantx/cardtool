using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CardType
{
    Hero_Red = 1,
    Hero_Green = 2,
    Hero_Blue = 3,
    Monster_Red = 4,
    Monster_Green = 5,
    Monster_Blue = 6,
    Spell = 7,
    Trap_Red = 8,

    Trap_Green = 9,

    Trap_Blue = 10,

    Trap_Neutral = 11,
    Envi = 12
}

public enum CardStylist
{
    None,
    Red,
    Green,
    Blue,
    Gray
}

public enum RarityType
{
    None,
    Common,
    Legendary,
    Mythic,
    Rare,
    Uncommon
}

public enum ParameterType
{
    Mana,
    ATK,
    durability,
    effect,
    HP
}
public class ResourceManager : MonoBehaviour
{
    public static Sprite LoadCard(CardType cardType)
    {
        var path = string.Format("Frame_{0}", cardType.ToString().ToLower());
        var a = Resources.Load<Sprite>(path);
        return a;
    }

    public static Sprite LoadRarity(RarityType rarity)
    {
        var path = string.Format("Card_rarity_{0}", rarity.ToString().ToLower());
        var a = Resources.Load<Sprite>(path);
        return a;
    }

    public static Sprite LoadIcon(CardType cardType, string cardName)
    {
        var cartTypeS = cardType.ToString().ToLower();
        switch (cardType)
        {
            case CardType.Hero_Red:
            case CardType.Hero_Green:
            case CardType.Hero_Blue:
                cartTypeS = "hero";
                break;
            case CardType.Monster_Red:
            case CardType.Monster_Green:
            case CardType.Monster_Blue:
                cartTypeS = "monster";
                break;
            case CardType.Spell:
                cartTypeS = "spell";
                break;
            case CardType.Trap_Red:
            case CardType.Trap_Green:
            case CardType.Trap_Blue:
            case CardType.Trap_Neutral:
                cartTypeS = "tool";
                break;
            case CardType.Envi:
                cartTypeS = "envi";
                break;
        }

        var path = string.Format("Icon/Pic_{0}_{1}", cartTypeS, cardName.Replace(" ", "_").ToLower());
        var a = Resources.Load<Sprite>(path);
        return a;
    }

    public static Sprite LoadParameter(ParameterType parameterType,bool special = false)
    {
        var infinite = special?"_infinite":"";
        var path = string.Format("Card_parameter_{0}{1}", parameterType.ToString(),infinite);
        var a = Resources.Load<Sprite>(path);
        return a;
    }


}
