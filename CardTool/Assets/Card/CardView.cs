using System.Collections;
using System.Collections.Generic;
using SFB;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardView : MonoBehaviour
{
    public Image Bg;
    public Image Icon;
    public Image ATK;
    public Image DEF;
    public Image Rarity;
    public Image Mana;

    public TextMeshProUGUI ATKValue;
    public TextMeshProUGUI DEFValue;
    public TextMeshProUGUI MANAValue;
    public TextMeshProUGUI NameCard;
    public TextMeshProUGUI Content;
    public TextMeshProUGUI Group;
    public RenderTexture RTexture;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeName(string name)
    {
        this.NameCard.text = name;
    }

    public void ChangeGroup(string group)
    {
        this.Group.text = group;
        this.Group.gameObject.SetActive(group != "");
    }

    private CardType _cardType = CardType.Hero_Red;

    public void ChangeCardType(CardType cardType)
    {
        this._cardType = cardType;
        this.Bg.sprite = ResourceManager.LoadCard(cardType);
    }

    public void ChangeRarity(RarityType rarity)
    {
        this.Rarity.sprite = ResourceManager.LoadRarity(rarity);
        this.Rarity.gameObject.SetActive(rarity!= RarityType.None);
    }

    public void ChangeStylist()
    {
        //TODO
    }

    public void ChangeParameterLeft(ParameterType parameterType, string value, bool active = false)
    {
        switch (parameterType)
        {
            case ParameterType.ATK:
                this.ATK.gameObject.SetActive(active);
                this.ATKValue.text = value;
                break;
            case ParameterType.Mana:
                this.Mana.gameObject.SetActive(active);
                this.MANAValue.text = value;
                break;
        }
    }

    public void ChangeParameterRight(ParameterType parameterType, string value)
    {
        this.DEF.sprite = ResourceManager.LoadParameter(parameterType,value == "0");
        this.DEFValue.text = value == "0"? "":value;
    }

    public void ChangeContent(string content)
    {
        this.Content.text = content;
    }

    public void ChangeIcon(string cardName)
    {
        this.Icon.sprite = ResourceManager.LoadIcon(this._cardType, cardName);
    }

    public void ChangeIconFromArt(string path)
    {
        this.LoadSpriteUrl(path, this.Icon);
    }

    public void ChangeScale(float scale){
        this.Icon.transform.localScale = Vector3.one*scale;
    }

    public void ChangePositionIcon(Vector3 pos)
    {
        this.Icon.transform.localPosition += pos*1.2f;
    }

    public void Save()
    {
        StartCoroutine(CoSave());
    }
    private IEnumerator CoSave()
    {
        //wait for rendering
        yield return new WaitForEndOfFrame();

        //set active texture
        RenderTexture.active = RTexture;

        //convert rendering texture to texture2D
        var texture2D = new Texture2D(RTexture.width, RTexture.height);
        texture2D.ReadPixels(new Rect(0, 0, RTexture.width, RTexture.height), 0, 0);
        texture2D.Apply();

        //write data to file
        var data = texture2D.EncodeToPNG();
        //string path = Application.persistentDataPath + "/" + NameCard.text + ".png";
        var path = StandaloneFileBrowser.SaveFilePanel("Save File", Application.persistentDataPath, NameCard.text , "png");
        if(path.Length==0) yield break;
        System.IO.File.WriteAllBytes(path, data);
        Application.OpenURL(path);
        Debug.Log(path);
    }

    void LoadSpriteUrl(string url, Image image)
    {
        StartCoroutine(ILoadSpriteUrl(url, image));
    }

    [System.Obsolete]
    IEnumerator ILoadSpriteUrl(string url, Image image)
    {
        WWW www = new WWW(url);
        yield return www;
        Texture2D texture = new Texture2D(1, 1);                //CREATE TEXTURE WIDTH = 1 AND HEIGHT =1
        www.LoadImageIntoTexture(texture);                      //LOAD DOWNLOADED TEXTURE TO VARIABEL TEXTURE
        Sprite sprite = Sprite.Create(texture,
                new Rect(0, 0, texture.width, texture.height),      //LOAD TEXTURE FROM POINT (0,0) TO TEXTURE(WIDTH, HEIGHT)
                Vector2.one / 2);
        Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        image.sprite = sprite;
        this.Icon.transform.localScale = Vector3.one;
        this.Icon.transform.localPosition = Vector3.zero;
        image.SetNativeSize();
    }
}
