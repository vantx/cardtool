using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIDraggable : MonoBehaviour, IDragHandler {


#region IDragHandler implementation

public Action<Vector3> OnChangePosition;

public void OnDrag (PointerEventData eventData)
{
    if(this.OnChangePosition != null)this.OnChangePosition((Vector3)eventData.delta);
}

#endregion
}